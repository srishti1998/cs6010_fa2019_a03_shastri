# SECTIONS
[REFLECTION](#reflection)

[REFLECTION QUESTIONS](#reflection-questions)



## REFLECTION

### *About the assignment:*

In this assignment we were given with two datasets- NFL and San Francisco Permits. We have to analyze these datasets by performing some functions on them. Then we have to find missing data in each dataset and the percentage of missing data in each field. We also have to figure out whether dropping all the missing data is a good option or not. Now as we found missing data the next step is to fill those missing data with atleast two methods

### *My Task:*

I used jupyter notebooks, python, pandas, Gitlab, Anaconda. I imported all the libraries and read the given data. 

I performed all the functions on the data such as head(),tail(),describe(),shape() and many more. After this I found the missing values using isna() and counted them using sum() and then found the percentage of missing values in each field.

I tried to drop all the missing values and then i figured out that dropping the missing values is not a good idea as it will also drop the values which is important. 

I used two methods to fill the missing values

1. filling with default value

2. filling with last known value


## REFLECTION QUESTIONS

> **Q1. What did I do well in this assignment?**

Ans. According to me I analyzed the data and found the missing values very well. And I think I did everything which is asked in assignment to do.

> **Q2. What was the most challenging part of this assignment?**

Ans. The most challenging part in the assignment was to deal with missing values and to use two methods to get rid of those missing values.

> **Q3. How can I improve on things that were very challenging to me?**

Ans. I can improve on things that were challenging to me by practicing more and by performing more methods to handle missing values.

> **Q4. What do I need help with?**

Ans. I need a little bit help with Git as well as in organization and presentation of assignment.


